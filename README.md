# Maschinen Verleih FFHS Projekt
## Setup
1. Installieren eines Docker clients
2. Starten der Services mit `docker-compose up`
3. Ausführen der Migrationen mit `docker-compose exec backend flask db upgrade`
4. Optional: Hinzufügen von Maschinen/Usern/Reservationen im Adminpanel unter localhost:8000/admin

## Techstack
Frontendtechnologie:
* React (JS)

UI-Frameworks:
* Reactstrap
* Handsontable
* React-Calendar
* Sweetalert2

Backend:
* Flask

Flask-Frameworks:
* SQLAlchemy
* Alembic
* Flask-Email
* Flask-User

DB:
* PostgreSQL

Environment:
* Docker

Deployment:
* Kubernetes
