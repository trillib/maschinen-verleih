from . import routes
from ..db import db
from ..models.machine import Machine
from ..models.reservation import Reservation
from ..serializers.machine_serializer import MachineSerializer
from ..config import BASE_URL
from flask import json


@routes.route(BASE_URL + "/machines", methods=['GET'])
def get_machines():
    machines = Machine.query.all()
    machines_serializer = MachineSerializer(many=True)
    return json.dumps({"machines": machines_serializer.dump(machines)})


@routes.route(BASE_URL + "/machines/<int:machine_id>", methods=['GET'])
def get_machine(machine_id):
    machine = Machine.query.filter_by(id=machine_id).first()
    machine_serializer = MachineSerializer()
    return json.dumps({"machine": machine_serializer.dump(machine)})


@routes.route(BASE_URL + "/reservations/<int:reservation_id>/machine", methods=['GET'])
def get_machine_for_reservation(reservation_id):
    try:
        reservation = Reservation.query.get(reservation_id)
        machine = reservation.machine
        machine_serializer = MachineSerializer()
        return json.dumps({"machine": machine_serializer.dump(machine)})
    except: 
        return json.dumps({"info": "Machine for this reservation could not be found"}), 404