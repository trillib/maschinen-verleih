from . import routes
from ..db import db
from ..models.machine import Machine
from ..models.reservation import Reservation
from ..serializers.machine_serializer import MachineSerializer
from ..config import BASE_URL
from flask import json, request
from flask_jwt_extended import jwt_required
from ..helpers.auth_helper import is_admin
from ..helpers.query_helper import get_years_from_reservations, get_revenue_for_year, get_number_of_users, get_total_cost_for_user_and_year, get_reservations_with_machine_infos
from ..serializers.reservation_machine_serializer import ReservationMachineSerializer
from sqlalchemy.sql import func


@routes.route(BASE_URL + "/admin/years", methods=['GET'])
@jwt_required
@is_admin
def get_years():
    years = get_years_from_reservations()
    return json.dumps({"years": years})


@routes.route(BASE_URL + "/admin/statistics", methods=['GET'])
@jwt_required
@is_admin
def get_statistics():
    year = request.args.get('year')
    revenue = get_revenue_for_year(year)
    number_of_user = get_number_of_users()
    return json.dumps({"statistics": {"revenue": revenue, "number_of_users": number_of_user}})


@routes.route(BASE_URL + "/admin/users/<int:user_id>/reservations", methods=['GET'])
@jwt_required
@is_admin
def get_reservations_for_user_for_admin(user_id):
    year = request.args.get('year')
    reservations_for_user = get_reservations_with_machine_infos(user_id, year)
    print(reservations_for_user)
    reservation_machine_serizalizer = ReservationMachineSerializer(many=True)
    return json.dumps({"reservations": reservation_machine_serizalizer.dump(reservations_for_user)})


@routes.route(BASE_URL + "/admin/users/<int:user_id>/reservations/total", methods=['GET'])
@jwt_required
@is_admin
def get_total_cost_for_user(user_id):
    year = request.args.get('year')
    total_cost = get_total_cost_for_user_and_year(user_id, year)
    return json.dumps({"total_cost": total_cost})
