from . import routes
from flask import json, request
from ..models.user import User
from ..models.revoked_token import RevokedToken
from ..db import db
from ..helpers.errors import ValidationError, AuthenticationError
from ..helpers.auth_helper import validate_form_data
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)


@routes.route("/login", methods=["POST"])
def login():
    try:
        email, password = validate_form_data(request.json, register=False)
        current_user = User.find_by_email(email)
        if not current_user:
            raise AuthenticationError("User {} doesn't exist".format(email))
        if not User.hash_is_correct(password, current_user.password):
            raise AuthenticationError("Wrong credentials")

        access_token = create_access_token(identity=email)
        refresh_token = create_refresh_token(identity=email)
        return json.dumps({
            "message": "Logged in as {}".format(current_user.name),
            "user_id": current_user.id,
            "is_admin": current_user.admin,
            "access_token": access_token,
            "refresh_token": refresh_token
        })
    except ValidationError as e:
        error_code = e.args[0]
        return json.dumps({"error": error_code}), 415
    except AuthenticationError as e:
        error_code = e.args[0]
        return json.dumps({"error": error_code}), 403


@routes.route("/register", methods=["POST"])
def register():
    try:
        email, password, name = validate_form_data(request.json, register=True)
        if User.find_by_email(email):
            raise ValidationError(
                "Nutzer mit dieser E-Mail bereits hinterlegt")
    except ValidationError as e:
        error_code = e.args[0]
        return json.dumps({"error": error_code}), 415
    new_user = User(
        email=email,
        name=name,
        password=User.generate_hash(password)
    )
    try:
        new_user.save()
        access_token = create_access_token(identity=email)
        refresh_token = create_refresh_token(identity=email)
        return json.dumps({
            "message": "User {} was created".format(email),
            "user_id": new_user.id,
            "access_token": access_token,
            "is_admin": new_user.admin,
            "refresh_token": refresh_token
        })
    except Exception as e:
        print(e)
        return json.dumps({"message": "Something went wrong"}), 500


@routes.route("/refresh_token", methods=["POST"])
@jwt_refresh_token_required
def refresh_token():
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    return json.dumps({"access_token": access_token})


@routes.route("/logout_access_token", methods=["POST"])
@jwt_required
def logout_access_token():
    jti = get_raw_jwt()["jti"]
    try:
        revoked_token = RevokedToken(jti=jti)
        revoked_token.add()
        return json.dumps({"message": "Access token has been revoked"})
    except:
        return json.dumps({"message": "Something went wrong"}), 500


@routes.route("/logout_refresh_token", methods=["POST"])
@jwt_refresh_token_required
def logout_refresh_token():
    jti = get_raw_jwt()["jti"]
    try:
        revoked_token = RevokedToken(jti=jti)
        revoked_token.add()
        return json.dumps({"message": "Refresh token has been revoked"})
    except:
        return json.dumps({"message": "Something went wrong"}), 500
