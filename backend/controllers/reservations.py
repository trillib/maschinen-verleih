from datetime import datetime, timedelta
from . import routes
import hashlib 
from ..db import db
from ..models.reservation import Reservation
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sqlalchemy import or_, and_
from ..models.user import User
from ..serializers.reservation_serializer import ReservationSerializer
from marshmallow import ValidationError
from flask import request, json
from ..config import BASE_URL
from ..helpers.general import is_number
from ..helpers.query_helper import overlapping_count
from flask_jwt_extended import jwt_required, get_jwt_identity
from ..helpers.auth_helper import reservation_belongs_to_user, user_is_correct


@routes.route(BASE_URL + "/machines/<int:machine_id>/reservations", methods=['POST'])
@jwt_required
def new_reservation(machine_id):
    reservation_data = request.json
    reservation_serializer = ReservationSerializer()
    user_id = User.query.filter(User.email == get_jwt_identity()).first().id
    try:
        reservation = reservation_serializer.load(reservation_data)
        if(overlapping_count(reservation, machine_id) > 0):
            return json.dumps(
                {"errors": "Reservation überschneidet sich mit anderer Reservation"}
            ), 422
        reservation.machine_id = machine_id
        reservation.user_id = user_id
        reservation.save()
    except Exception as err: #ValidationError
        print(err)
        error_list = list(err.messages.values())
        error_list_flat = [val for sublist in error_list for val in sublist]
        return json.dumps({"errors": error_list_flat}), 422

    return json.dumps({"reservation": reservation_serializer.dump(reservation)})


@routes.route(BASE_URL + "/users/<int:user_id>/reservations", methods=["GET"])
@jwt_required
@user_is_correct
def get_reservations_for_user(user_id):
    reservations = Reservation.query.filter_by(user_id=user_id).all()
    reservation_serializer = ReservationSerializer(many=True)
    return json.dumps({"reservations": reservation_serializer.dump(reservations)})


@routes.route(BASE_URL + "/machines/<int:machine_id>/reservations", methods=['GET'])
def get_reservations_for_machine(machine_id):
    reservations = Reservation.query.filter_by(machine_id=machine_id).all()
    reservation_serializer = ReservationSerializer(many=True)

    reservations_with_user = []

    for reservation in reservation_serializer.dump(reservations):
        reservation_with_user = reservation
        userid = reservation['user_id']
        username = User.query.filter(User.id == userid).first().name
        reservation_with_user['username'] = username
        reservations_with_user.append(reservation_with_user)
        
    return json.dumps({"reservations": reservations_with_user})


@routes.route(BASE_URL + "/reservations/<int:reservation_id>/complete", methods=['POST'])
@jwt_required
@reservation_belongs_to_user
def complete_reservation(reservation_id):
    reservation = Reservation.query.get(reservation_id)
    json_data = request.json
    if reservation is None or reservation.status != "offen":
        return json.dumps({"errors": "Kein Zugriff auf diese Reservation"}), 403
    if json_data is None or json_data["usage"] is None:
        return json.dumps({"errors": "Keine Benützung angegeben"}), 422
    if not is_number(json_data["usage"]):
        return json.dumps({"errors": "Benützungsdaten sind im falschen Format"}), 422
    reservation.status = "abgeschlossen"
    reservation.usage = json_data["usage"]
    reservation_serializer = ReservationSerializer()
    db.session.commit()
    return json.dumps({"reservation": reservation_serializer.dump(reservation)}), 200


@routes.route(BASE_URL + "/reservations/<int:reservation_id>/update", methods=['POST'])
@jwt_required
@reservation_belongs_to_user
def update_reservation(reservation_id):
    reservation = Reservation.query.filter_by(id=reservation_id).first()
    json_data = request.json
    if reservation is None:
        return json.dumps({"errors": "Reservation nicht gefunden"}), 404
    reservation.start = json_data["start"]
    reservation.end = json_data["end"]
    reservation_serializer = ReservationSerializer()
    if(overlapping_count(reservation, reservation.machine_id, True) > 0):
        return json.dumps(
            {"errors": "Reservation überschneidet sich mit anderer Reservation"}
        ), 422
    db.session.commit()
    return json.dumps({"reservation": reservation_serializer.dump(reservation)}), 200


@routes.route(BASE_URL + "/reservations/<int:reservation_id>/delete", methods=['DELETE'])
@jwt_required
@reservation_belongs_to_user
def delete_reservation(reservation_id):
    reservation = Reservation.query.filter_by(id=reservation_id).first()
    if reservation is None:
        return json.dumps({"errors": "Reservation nicht gefunden"}), 404
    db.session.delete(reservation)
    db.session.commit()
    return json.dumps({"info": "Deletion successful"}), 200


@routes.route(BASE_URL + "/reservations/remind", methods=['POST'])
def remind_users():
    SENDGRID_API_KEY='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'
    real_pw = 'pleasePlaceThisToENV'

    def send_mail(to_mail_address):
        message = Mail(
            from_email='my-machine@my-machine.io',
            to_emails=to_mail_address,
            subject='Sending with Twilio SendGrid is Fun',
            html_content='<strong>and easy to do anywhere, even with Python</strong>')
        try:
            sg = SendGridAPIClient(SENDGRID_API_KEY)
            response = sg.send(message)
        except Exception as e:
            print(e.message)

    password = request.form['password']
    
    if password == real_pw:

        try:
            five_days_ago = datetime.now() - timedelta(hours=120)
            res_5d = Reservation.query.filter(
                    and_(Reservation.status != "abgeschlossen",
                        Reservation.end <= five_days_ago,
                        Reservation.last_reminder_sent == None)
            ).all()

            two_days_ago = datetime.now() - timedelta(hours=48) 
            res_2d = Reservation.query.filter(
                    and_(Reservation.status != "abgeschlossen",
                        Reservation.last_reminder_sent != None,
                        Reservation.last_reminder_sent <= two_days_ago)
            ).all()

            reservations = res_5d + res_2d
            reservations = [x for x in reservations if x != []]
            
            if reservations:
                emails = set()
                for reservation in reservations:
                    email = reservation.user.email
                    emails.add(email)

                for email in emails:
                    send_mail(email)

                for reservation in reservations:
                    reservation.last_reminder_sent = datetime.now()
                else:
                    db.session.commit()
                
                return json.dumps({"info": "Reminders sent"}), 200

            return json.dumps({"info": "No reminders sent (noone to remind)"}), 200
        except:
            return json.dumps({"info": "An error occured doring sending of reminder emails"}), 400
    
    return json.dumps({"info": "Unauthorized"}), 401


    