from flask_admin import Admin
from ..models.model_view import AdminView
from ..app import app
from ..db import db
from ..models.machine import Machine
from ..models.reservation import Reservation
from ..models.user import User


admin_page = Admin(app, name='maschinen', template_mode='bootstrap3')
admin_page.add_view(AdminView(Machine, db.session))
admin_page.add_view(AdminView(Reservation, db.session))
admin_page.add_view(AdminView(User, db.session))