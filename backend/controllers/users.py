from . import routes
from ..models.user import User
from ..config import BASE_URL
from flask import json
from ..serializers.user_serializer import UserSerializer
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
from ..models.user import User
from ..helpers.auth_helper import is_admin, user_is_correct


@routes.route(BASE_URL + "/users", methods=['GET'])
@jwt_required
@is_admin
def get_users():
    users = User.query.all()
    users_serializer = UserSerializer(many=True)
    return json.dumps({"users": users_serializer.dump(users)})


@routes.route(BASE_URL + "/users/<int:user_id>", methods=['GET'])
@jwt_required
@user_is_correct
def get_user(user_id):
    user = User.query.filter(User.id == user_id).all()[0]
    user_serializer = UserSerializer()
    return json.dumps({"user": user_serializer.dump(user)})
