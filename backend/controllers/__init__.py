from flask import Blueprint
routes = Blueprint("routes", __name__)

from .machines import *
from .reservations import *
from .users import *
from .admin_panel import *
from .admin import *
from .auth import *
