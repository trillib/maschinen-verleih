from ..models.reservation import Reservation
from ..models.machine import Machine
from ..models.user import User
from ..db import db
from sqlalchemy import or_, and_, extract, text, func
import itertools


def overlapping_count(reservation, machine_id, update=False):
    if(update):
        return Reservation.query.filter(
        Reservation.machine_id == machine_id,
        Reservation.id != reservation.id,
        or_(
            and_(Reservation.start <= reservation.start,
                 Reservation.end >= reservation.start),
            and_(Reservation.start <= reservation.end,
                 Reservation.end >= reservation.end),
            and_(Reservation.start >= reservation.start,
                 Reservation.end <= reservation.end))
        ).count()

    else:
        return Reservation.query.filter(
        Reservation.machine_id == machine_id,
        or_(
            and_(Reservation.start <= reservation.start,
                 Reservation.end >= reservation.start),
            and_(Reservation.start <= reservation.end,
                 Reservation.end >= reservation.end),
            and_(Reservation.start >= reservation.start,
                 Reservation.end <= reservation.end))
        ).count()



def get_years_from_reservations():
    reservations = Reservation.query.with_entities(
        extract('year', Reservation.end)).distinct().all()
    flattened = list(itertools.chain.from_iterable(reservations))
    integers = [int(i) for i in flattened]
    return integers


def get_revenue_for_year(year):
    query = "select sum(price*usage) from reservation join machine on reservation.machine_id=machine.id where reservation.status='abgeschlossen' and extract(year from reservation.end)=:year"
    pointer = db.session.execute(text(query), {"year": year})
    revenue = pointer.fetchone()[0]
    return revenue


def get_number_of_users():
    count = User.query.count()
    return count


def get_total_cost_for_user_and_year(user_id, year=None):
    query = db.session.query(func.sum(Reservation.usage * Machine.price)).join(
        Machine, Reservation.machine_id == Machine.id).filter(Reservation.user_id == user_id)
    if(year is not None):
        query = query.filter(
            extract('year', Reservation.end) == year)
    return query.all()[0][0]


def get_reservations_with_machine_infos(user_id, year=None):
    query = db.session.query(Reservation.start, Reservation.end, Reservation.id, Machine.name, Machine.billing_type, (Machine.price * Reservation.usage).label("cost")).join(
        Machine).filter(Reservation.user_id == user_id).order_by(Reservation.end.asc())
    if(year is not None):
        query = query.filter(
            extract('year', Reservation.end) == year)
    return query.all()
