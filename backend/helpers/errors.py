from werkzeug.exceptions import HTTPException
from flask import Response

class ValidationError(Exception):
    pass


class AuthenticationError(Exception):
    pass


class AuthException(HTTPException):
    def __init__(self, message):
        super().__init__(message, Response(
            "You could not be authenticated. Please refresh the page.", 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        ))
