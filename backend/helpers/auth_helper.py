from .errors import ValidationError
from functools import wraps
from flask import json, request
from flask_jwt_extended import get_jwt_identity
from ..models.user import User
from ..models.reservation import Reservation
from functools import wraps
import inspect


def validate_form_data(form_data, register=False):
    email = form_data.get('email')
    if email is None:
        raise ValidationError("Bitte E-Mail angeben")
    password = form_data.get('password')
    if password is None:
        raise ValidationError("Bitte Passwort angeben")
    if register:
        name = form_data.get("name")
        if name is None:
            raise ValidationError("Bitte Namen angeben")
        return (email, password, name)
    else:
        return(email, password)


def is_admin(func):
    """
    Decorator checks if a user is admin
    If the user is admin he has the rights anyway
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        email = get_jwt_identity()
        user = User.query.filter(User.email == email).first()
        if(user.admin):
            return func(*args, **kwargs)
        return json.dumps({"error": "Unauthorized"}), 401
    return wrapper


def reservation_belongs_to_user(func):
    """
    Decorator checks if the reservation belongs to the logged in user
    If the user is admin he has the rights anyway
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        logged_in_email = get_jwt_identity()
        logged_in_user = User.query.filter(
            User.email == logged_in_email).first()

        requested_reservation_id = kwargs["reservation_id"]
        requested_reservation = Reservation.query.filter(
            Reservation.id == requested_reservation_id).first()
        if requested_reservation is not None:
            request_user_id = requested_reservation.user.id
        if logged_in_user.id == request_user_id or logged_in_user.admin:
            return func(*args, **kwargs)
        return json.dumps({"error": "Unauthorized"}), 401
    return wrapper


def user_is_correct(func):
    """
    That checks if the user_id in the request is the same as the logged in user.
    If the user is admin he has the rights anyway
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        user_id_from_request = kwargs["user_id"]
        logged_in_email = get_jwt_identity()
        logged_in_user = User.query.filter(
            User.email == logged_in_email).first()
        if logged_in_user.id == user_id_from_request or logged_in_user.admin:
            return func(*args, **kwargs)
        return json.dumps({"error": "Unauthorized"}), 401
    return wrapper
