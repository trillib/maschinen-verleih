from ..models.reservation import Reservation
from ..db import ma
from marshmallow import post_load, fields, Schema, validates_schema, ValidationError


class ReservationSerializer(Schema):
    id = fields.Integer()
    start = fields.DateTime(required=True)
    end = fields.DateTime(required=True)
    usage = fields.Float()
    status = fields.String()
    machine_id = fields.Integer()
    user_id = fields.Integer()

    @post_load
    def make_reservation(self, data):
        return Reservation(**data)

    @validates_schema
    def validate_numbers(self, data):
        if data['start'] >= data['end']:
            raise ValidationError('end must be greater than start')
