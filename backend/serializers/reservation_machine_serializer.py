from ..models.reservation import Reservation
from ..db import ma
from marshmallow import post_load, fields, Schema, validates_schema, ValidationError


class ReservationMachineSerializer(Schema):
    id = fields.Integer()
    start = fields.DateTime(required=True)
    end = fields.DateTime(required=True)
    usage = fields.Float()
    status = fields.String()
    name = fields.String()
    billing_type = fields.String()
    cost = fields.String()