from ..models.machine import Machine
from ..db import ma
from marshmallow import post_load, fields, Schema


class MachineSerializer(Schema):
    id = fields.Integer()
    name = fields.String()
    description = fields.String()
    billing_type = fields.String()
    price = fields.Float()
    location = fields.String()

    @post_load
    def make_machine(self, data):
        return Machine(**data)


