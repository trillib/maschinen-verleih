from ..models.user import User
from ..db import ma
from marshmallow import  fields, Schema


class UserSerializer(Schema):
    id = fields.Integer()
    name = fields.String()
    email = fields.String()
