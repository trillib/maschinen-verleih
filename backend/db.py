from flask_sqlalchemy import SQLAlchemy
from .config import DATABASES as DB_CONFIG
from .app import app
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
from flask_basicauth import BasicAuth

POSTGRES_USER = DB_CONFIG['default']['USER']
POSTGRES_PW = DB_CONFIG['default']['PASSWORD']
POSTGRES_URL = '{}:{}'.format(
    DB_CONFIG['default']['HOST'], str(DB_CONFIG['default']['PORT']))
POSTGRES_DB = DB_CONFIG['default']['NAME']

DB_URI = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}' \
    .format(user=POSTGRES_USER,
            pw=POSTGRES_PW,
            url=POSTGRES_URL,
            db=POSTGRES_DB)

app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI

db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)

