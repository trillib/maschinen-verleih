"""empty message

Revision ID: 41e6c3a6d032
Revises: 1940f5b3295e
Create Date: 2019-05-31 12:40:44.759534

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '41e6c3a6d032'
down_revision = '1940f5b3295e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('reservation', sa.Column('last_reminder_sent', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('reservation', 'last_reminder_sent')
    # ### end Alembic commands ###
