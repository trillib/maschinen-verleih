from . import app
from . import db
from . import basic_auth
from .controllers import *
from flask_jwt_extended import JWTManager
from .models.revoked_token import RevokedToken


# Initialize Controllers
app.register_blueprint(routes)

# Initialize JWT
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """
    is called every time client try to access secured endpoints. Function under the decorator should return True or False depending on if the passed token is blacklisted.
    """
    jti = decrypted_token['jti']
    return RevokedToken.is_jti_blacklisted(jti)