DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgress',
        'HOST': 'db',
        'PORT': 5432,
    }
}

BASE_PATH = "/api/"
API_VERSION = "v1"

BASE_URL = BASE_PATH + API_VERSION