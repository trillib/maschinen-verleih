from flask_admin.contrib import sqla
from ..basic_auth import basic_auth
from flask import redirect, Response


class AdminView(sqla.ModelView):
    def is_accessible(self):
        if not basic_auth.authenticate():
            return Response(
                'Could not verify your access level for that URL.\n'
                'You have to login with proper credentials', 401,
                {'WWW-Authenticate': 'Basic realm="Login Required"'})
        else:
            return True

    def inaccessible_callback(self, name, **kwargs):
        print("in")
        return redirect(basic_auth.challenge())
