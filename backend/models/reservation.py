from ..db import db


class Reservation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start = db.Column(db.DateTime)
    end = db.Column(db.DateTime)
    usage = db.Column(db.Float)
    status = db.Column(db.String(45), default="offen")
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    machine_id = db.Column(db.Integer, db.ForeignKey('machine.id'))
    last_reminder_sent = db.Column(db.DateTime)
    user = db.relationship("User")
    machine = db.relationship("Machine")
    

    def save(self):
        db.session.add(self)
        db.session.commit()
