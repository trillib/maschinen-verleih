from ..db import db
from passlib.hash import pbkdf2_sha256 as sha256

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45))
    email = db.Column(db.String(90), unique=True)
    unionist = db.Column(db.Boolean)
    password = db.Column(db.String(180))
    admin = db.Column(db.Boolean, default=False)

    def save(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, name):
        return cls.query.filter_by(name = name).first()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email = email).first()

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)
        
    @staticmethod
    def hash_is_correct(password, hash):
        return sha256.verify(password, hash)
