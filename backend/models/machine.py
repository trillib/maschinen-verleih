from ..db import db


class Machine(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), index=True)
    description = db.Column(db.String(360))
    billing_type = db.Column(db.String(45))
    location = db.Column(db.String(90))
    price = db.Column(db.Float())
    reservations = db.relationship("Reservation")

    def save(self):
        db.session.add(self)
        db.session.commit()
