import React, { Fragment } from "react";
import PropTypes from "prop-types";

const StatisticsBlock = ({ revenue, userCount }) => (
  <Fragment>
    <h2>Statistiken</h2>
    <p>Jahresumsatz: {revenue} CHF</p>
    <p>Nutzer: {userCount} Nutzer</p>
  </Fragment>
);

StatisticsBlock.propTypes = {
  revenue: PropTypes.number.isRequired,
  userCount: PropTypes.number.isRequired
};

export default StatisticsBlock;
