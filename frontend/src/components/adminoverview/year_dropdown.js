import React from "react";
import {
  Dropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from "reactstrap";
import PropTypes from "prop-types";

const YearDropdown = ({
  dropdownOpen,
  toggleDropdown,
  dropdownItems,
  setDropdownValue,
  selectedDropdownValue
}) => (
  <Dropdown
    isOpen={dropdownOpen}
    toggle={toggleDropdown}
    className="float-right"
    tag="span"
  >
    <DropdownToggle caret>{selectedDropdownValue}</DropdownToggle>
    <DropdownMenu>
      {dropdownItems.map((item, index) => (
        <DropdownItem onClick={(event) => setDropdownValue(event.target.textContent)} key={index}>
          {item}
        </DropdownItem>
      ))}
    </DropdownMenu>
  </Dropdown>
);

YearDropdown.propTypes = {
  dropdownOpen: PropTypes.bool.isRequired,
  toggleDropdown: PropTypes.func.isRequired,
  dropdownItems: PropTypes.array.isRequired,
  setDropdownValue: PropTypes.func.isRequired,
  selectedDropdownValue: PropTypes.number.isRequired
};

export default YearDropdown;
