import React from "react";
import PropTypes from "prop-types";
import { Table, Button } from "reactstrap";

const UserTable = ({ tableData, detailAction }) => (
  <Table striped>
    <thead>
      <tr>
        <th>Name</th>
        <th>E-Mail</th>
        <th />
      </tr>
    </thead>
    <tbody>
      {tableData.map((user, index) => (
        <tr key={index}>
          <td>{user.name}</td>
          <td>{user.email}</td>
          <td>
            <Button onClick={_ => detailAction(user.id, user.name)}>
              Details <i className="fa fa-info-circle" />
            </Button>
          </td>
        </tr>
      ))}
    </tbody>
  </Table>
);

UserTable.propTypes = {
  detailAction: PropTypes.func.isRequired,
  tableData: PropTypes.array.isRequired,
};

export default UserTable;
