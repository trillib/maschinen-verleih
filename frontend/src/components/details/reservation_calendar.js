import React, { Component } from "react";
import Calendar from "react-calendar";
import "../../styles/calendar_styles.css";
import PropTypes from "prop-types";
import moment from "moment";
import "moment/locale/de";

class ReservationCalendar extends Component {
  state = {
    date: new Date()
  };

  onChange = date => {
    this.setState({ date });
    this.props.onClick(date);
  };

  setFullDates = ({ date, view }) => {
    let dates = [];
    let durations = {};
    for (var i = 0; i < this.props.reservations.length; i++) {
      durations[this.props.reservations[i].date] = this.props.reservations[
        i
      ].duration;
      dates.push(this.props.reservations[i].date);
    }

    if (
      view === "month" &&
      dates.indexOf(moment(date).format("DD.MM.YYYY")) >= 0
    ) {
      if (durations[moment(date).format("DD.MM.YYYY")] >= 4) {
        return "full";
      } else {
        return "half-full";
      }
    }
  };

  render() {
    return (
      <div>
        <Calendar
          className="reservation-calendar"
          onChange={this.onChange}
          value={this.state.date}
          tileClassName={this.setFullDates}
          reservations={this.props.reservations}
        />
      </div>
    );
  }
}

ReservationCalendar.propTypes = {
  onClick: PropTypes.func.isRequired,
  reservations: PropTypes.array.isRequired
};

export default ReservationCalendar;
