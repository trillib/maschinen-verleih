import React from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  ListGroup,
  ListGroupItem
} from "reactstrap";
import PropTypes from "prop-types";

const ReservationOverviewModal = ({
  reservations,
  machineName,
  isModalOpen,
  newReservation,
  closeModal,
  selectedDate, 
  selDate
}) => (
  <Modal isOpen={isModalOpen}>
    <ModalHeader toggle={closeModal}>
      {machineName} - {selectedDate}
    </ModalHeader>
    <ModalBody>
      <ListGroup>
        {reservations && reservations.filter(
          (reservation) => (reservation.date === selDate)).map((reservation, index) => (
            <ListGroupItem key={index}>
             {reservation.time} Uhr von {reservation.reserved_by}
            </ListGroupItem>
        ))}
      </ListGroup>
    </ModalBody>
    <ModalFooter>
      <Button color="success" onClick={newReservation}>
        Neue Reservation
      </Button>{" "}
    </ModalFooter>
  </Modal>
);

ReservationOverviewModal.propType = {
  machineName: PropTypes.string,
  isModalOpen: PropTypes.bool.isRequired,
  newReservation: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  reservations: PropTypes.array.isRequired,
  selectedDate: PropTypes.string.isRequired,
  selDate: PropTypes.string.isRequired
};

export default ReservationOverviewModal;
