import React, { Fragment } from "react";
import { FormGroup, Label } from "reactstrap";
import PropTypes from "prop-types";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import de from "date-fns/locale/de";

registerLocale("de", de);
const ReservationForm = ({
  handleChange,
  fromDate,
  toDate,
  fromTime,
  toTime
}) => (
  <Fragment>
    <FormGroup>
      <Label for="from_date">Von Datum</Label>
      <br />
      <DatePicker
        className="form-control"
        onChange={date => handleChange("fromDate", date)}
        id="from_date"
        selected={fromDate}
        locale="de"
        dateFormat="dd. MMM yyyy"
      />
    </FormGroup>
    <FormGroup>
      <Label for="from_time">Von Zeit</Label>
      <br />
      <DatePicker
        className="form-control"
        onChange={time => handleChange("fromTime", time)}
        id="from_time"
        selected={fromTime}
        locale="de"
        dateFormat="HH:mm"
        timeFormat="HH:mm"
        showTimeSelect
        showTimeSelectOnly
        timeIntervals={15}
        timeCaption="Zeit"
      />
    </FormGroup>

    <FormGroup>
      <Label for="to_date">Bis</Label>
      <br />
      <DatePicker
        className="form-control"
        onChange={date => handleChange("toDate", date)}
        id="from_date"
        selected={toDate}
        locale="de"
        dateFormat="dd. MMM yyyy"
      />
    </FormGroup>
    <FormGroup>
      <Label for="to_time">Von Zeit</Label>
      <br />
      <DatePicker
        className="form-control"
        onChange={time => handleChange("toTime", time)}
        id="to_time"
        selected={toTime}
        locale="de"
        dateFormat="HH:mm"
        timeFormat="HH:mm"
        showTimeSelect
        showTimeSelectOnly
        timeIntervals={15}
        timeCaption="Zeit"
      />
    </FormGroup>
  </Fragment>
);

ReservationForm.propType = {
  handleChange: PropTypes.func.isRequired,
  fromDate: PropTypes.string,
  toDate: PropTypes.string,
  fromTime: PropTypes.string,
  toTime: PropTypes.string
};

export default ReservationForm;
