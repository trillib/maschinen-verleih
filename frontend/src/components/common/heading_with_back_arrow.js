import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const HeadingWithBackArrow = ({ title, url }) => (
  <h1 style={{marginBottom: "15px"}}>
    <Link to={url}>
      <i className="fa fa-arrow-left" />
    </Link>
    {title}
  </h1>
);

HeadingWithBackArrow.propType = {
  title: PropTypes.string,
  url: PropTypes.string
};

export default HeadingWithBackArrow;
