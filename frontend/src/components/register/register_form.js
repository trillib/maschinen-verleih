import React, { Fragment } from "react";
import { Col, FormGroup, Label, Input, Button } from "reactstrap";
import { FormValidation } from "calidation";
import { PropTypes } from "prop-types";

const config = {
  email: {
    isRequired: "Bitte E-Mail angeben",
    isEmail: "Bitte gültige E-Mail angeben"
  },
  firstname: {
    isRequired: "Bitte Vorname angeben",
  },
  lastname: {
    isRequired: "Bitte Nachname angeben",
  },
  password: {
    isRequired: "Bitte Passwort angeben"
  },
  repeatPassword: {
    isRequired: 'Bitte Passwort ein zweites mal ausfüllen',
    isEqual: ({ fields }) => ({
        message: 'Die Passwörter müssen übereinstimmen',
        value: fields.password,
        validateIf: fields.password.length > 0, // this can be a boolean too!
    }),
}

};

export const RegisterForm = props => (
  <FormValidation onSubmit={result => props.submit(result)} config={config}>
    {({ errors, submitted }) => (
      <Fragment>
        <FormGroup row>
          <Label for="email-input" sm={2}>
            E-Mail
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.email
                ? { className: "is-invalid" }
                : {})}
              name="email"
              placeholder="E-Mail"
              id="email-input"
            />
            {submitted && errors.email && (
              <small id="emailHelp" className="text-danger">
                {errors.email}
              </small>
            )}
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="firstname-input" sm={2}>
            Vorname
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.firstname
                ? { className: "is-invalid" }
                : {})}
              name="firstname"
              placeholder="Vorname"
              id="firstname-input"
            />
            {submitted && errors.firstname && (
              <small id="firstnameHelp" className="text-danger">
                {errors.firstname}
              </small>
            )}
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="lastname-input" sm={2}>
            Nachname
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.lastname
                ? { className: "is-invalid" }
                : {})}
              name="lastname"
              placeholder="Nachname"
              id="lastname-input"
            />
            {submitted && errors.lastname && (
              <small id="lastnameHelp" className="text-danger">
                {errors.lastname}
              </small>
            )}
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="password-input" sm={2}>
            Password
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.password
                ? { className: "is-invalid" }
                : {})}
              type="password"
              name="password"
              placeholder="Passwort"
              id="password-input"
            />
            {submitted && errors.password && (
              <small id="passwordHelp" className="text-danger">
                {errors.password}
              </small>
            )}
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="repeatPassword-input" sm={2}>
            Passwort wiederholen
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.repeatPassword
                ? { className: "is-invalid" }
                : {})}
              type="password"
              name="repeatPassword"
              placeholder="Passwort wiederholen"
              id="repeatPassword-input"
            />
            {submitted && errors.repeatPassword && (
              <small id="repeatPasswordHelp" className="text-danger">
                {errors.repeatPassword}
              </small>
            )}
          </Col>
        </FormGroup>
        <Button type="submit">Submit</Button>
      </Fragment>
    )}
  </FormValidation>
);

RegisterForm.propTypes = {
  submit: PropTypes.func.isRequired
};
