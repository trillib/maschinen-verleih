import React, { Fragment } from "react";
import { Col, FormGroup, Label, Input, Button } from "reactstrap";
import { FormValidation } from "calidation";
import { PropTypes } from "prop-types";

const config = {
  email: {
    isRequired: "Bitte E-Mail angeben",
    isEmail: "Bitte gültige E-Mail angeben"
  },
  password: {
    isRequired: "Bitte Passwort angeben"
  }
};

export const LoginForm = props => (
  <FormValidation onSubmit={result => props.submit(result)} config={config}>
    {({ errors, submitted }) => (
      <Fragment>
        <FormGroup row>
          <Label for="email-input" sm={2}>
            Email
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.email
                ? { className: "is-invalid" }
                : {})}
              name="email"
              placeholder="email"
              id="email-input"
            />
            {submitted && errors.email && (
              <small id="emailHelp" className="text-danger">
                {errors.email}
              </small>
            )}
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="password-input" sm={2}>
            Password
          </Label>
          <Col sm={10}>
            <Input
              {...(submitted && errors.password
                ? { className: "is-invalid" }
                : {})}
              type="password"
              name="password"
              placeholder="password"
              id="password-input"
            />
            {submitted && errors.password && (
              <small id="passwordHelp" className="text-danger">
                {errors.password}
              </small>
            )}
          </Col>
        </FormGroup>
        <Button type="submit">Submit</Button>
      </Fragment>
    )}
  </FormValidation>
);

LoginForm.propTypes = {
  submit: PropTypes.func.isRequired
};
