import React from "react";
import PropTypes from "prop-types";
import { FormGroup, Input } from "reactstrap";

const SearchForm = ({ searchFunction }) => (
  <FormGroup className="has-feedback">
    <Input
      type="search"
      name="search"
      id="searchbar"
      placeholder="search"
      onChange={content => searchFunction(content)}
    />
  </FormGroup>
);

SearchForm.propTypes = {
  searchFunction: PropTypes.func.isRequired
};

export default SearchForm