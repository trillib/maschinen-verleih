import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem } from "reactstrap";

const HomeList = ({ items }) => (
  <ListGroup>
    {items.map((item, idx) => (
      <ListGroupItem key={idx}>
        <Link
          to={{
            pathname: `machines/${item.id}`,
            state: { machine: item }
          }}
        >
          {item.name}
        </Link>

        <span className="float-right">
          <i className="fa fa-home" />
          {item.location}
        </span>
      </ListGroupItem>
    ))}
  </ListGroup>
);

HomeList.propTypes = {
  items: PropTypes.array.isRequired
};

export default HomeList;
