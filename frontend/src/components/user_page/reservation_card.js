import React from "react";
import PropTypes from "prop-types";
import {
  Card,
  CardBody,
  CardTitle,
  Button,
  CardSubtitle,
  ButtonGroup
} from "reactstrap";

const ReservationCard = ({
  startDate,
  endDate,
  startTime,
  endTime,
  type,
  machineName,
  id,
  completeReservation,
  updateReservation,
  deleteReservation
}) => (
  <Card>
    <CardBody>
      <CardTitle>
        <h4>{machineName}</h4>
      </CardTitle>
      <CardSubtitle>
        {startDate} {startTime} - {endDate} {endTime}
      </CardSubtitle>
      {type === "zukünftig" && (
        <ButtonGroup className="d-flex user-action-button">
          <Button
            outline
            color="secondary"
            className="w-100"
            onClick={() => updateReservation(id)}
          >
            Bearbeiten
          </Button>
          <Button
            outline
            color="danger"
            className="w-100"
            onClick={() => deleteReservation(id)}
          >
            Löschen
          </Button>
        </ButtonGroup>
      )}
      {type === "vergangen" && (
        <Button
          className="user-action-button"
          outline
          color="success"
          block
          onClick={() => completeReservation(id)}
        >
          Abschliessen
        </Button>
      )}
    </CardBody>
  </Card>
);

ReservationCard.propTypes = {
  startDate: PropTypes.string.isRequired,
  startTime: PropTypes.string.isRequired,
  endTime: PropTypes.string.isRequired,
  endDate: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  machineName: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  completeReservation: PropTypes.func,
  updateReservation: PropTypes.func,
  deleteReservation: PropTypes.func
};
export default ReservationCard;
