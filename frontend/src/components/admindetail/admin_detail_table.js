import React from "react";
import PropTypes from "prop-types";
import { Table } from "reactstrap";
import moment from "moment";

const AdminDetailTable = ({ reservationData, totalCost }) => (
  <Table striped>
    <thead>
      <tr>
        <th />
        <th>Datum</th>
        <th>Maschine</th>
        <th>Kosten</th>
      </tr>
    </thead>
    <tbody>
      {reservationData.map((reservation, index) => (
        <tr key={index}>
          <th scope="row">{index + 1}</th>
          <td>{moment(reservation.start).format("ll")}</td>
          <td>{reservation.name}</td>
          <td>
            {reservation.cost !== null
              ? reservation.cost + "CHF"
              : "nicht abgeschlossen"}
          </td>
        </tr>
      ))}
      <tr>
        <th scope="row" style={{ textDecoration: "underline" }}>
          Total
        </th>
        <td />
        <td />
        <td>{totalCost} CHF</td>
      </tr>
    </tbody>
  </Table>
);

AdminDetailTable.propTypes = {
  reservationData: PropTypes.array.isRequired,
  totalCost: PropTypes.number.isRequired
};

export default AdminDetailTable;
