export const baseUrl = "/api/v1";

export class AuthorizedApiHandler {
  constructor(access_token, refresh_token, refresh_access_token_callback) {
    this.access_token = access_token;
    this.refresh_token = refresh_token;
    this.refresh_access_token_callback = refresh_access_token_callback;
  }

  getMachines = async () => {
    return await this.basicGet("/machines");
  };

  getMachine = async id => {
    return await this.basicGet(`/machines/${id}`);
  };

  getReservationsForMachine = async machineId => {
    return await this.basicGet(`/machines/${machineId}/reservations`);
  };

  getReservationsForUser = async userId => {
    return await this.basicGet(`/users/${userId}/reservations`);
  };

  getMachineForReservation = async reservationId => {
    return await this.basicGet(`/reservations/${reservationId}/machine`);
  };

  getUsers = async () => {
    return await this.basicGet(`/users`);
  };

  getYears = async () => {
    return await this.basicGet(`/admin/years`);
  };

  getStatistics = async year => {
    return await this.basicGet(`/admin/statistics?year=${year}`);
  };

  getReservationsForUserAdmin = async (user_id, year) => {
    return await this.basicGet(
      `/admin/users/${user_id}/reservations?year=${year}`
    );
  };

  getTotalCostForUser = async (user_id, year) => {
    return await this.basicGet(
      `/admin/users/${user_id}/reservations/total?year=${year}`
    );
  };

  getUser = async user_id => {
    return await this.basicGet(`/users/${user_id}`);
  };

  createNewReservation = async (machine_id, from_datetime, to_datetime) => {
    const url = baseUrl + "/machines/" + machine_id + "/reservations";
    const body = { start: from_datetime, end: to_datetime };
    return this.basicPost(url, JSON.stringify(body));
  };

  completeReservation = async (reservationId, usage) => {
    const url = baseUrl + "/reservations/" + reservationId + "/complete";
    const body = { usage };
    return this.basicPost(url, JSON.stringify(body));
  };

  logout = async () => {
    await this.logout_access_token();
    return await this.logout_refresh_token();
  };

  logout_access_token = async () => {
    const url = "/logout_access_token";
    return this.basicPost(url);
  };

  logout_refresh_token = async () => {
    const url = "/logout_refresh_token";
    return this.basicPost(url, {}, "application/json", true);
  };

  refresh_token_call = async () => {
    const url = "/refresh_token";
    return this.basicPost(url, {}, "application/json", true);
  };

  update_access_token = async () => {
    const refresh_token_response = await this.refresh_token_call();
    if (refresh_token_response.response) {
      const new_access_token = refresh_token_response.response.access_token;
      this.access_token = new_access_token;
      this.refresh_access_token_callback(
        refresh_token_response.response.access_token
      );
    }
  };

  basicGet = async url => {
    const response = await fetch(baseUrl + url, {
      headers: {
        Authorization: "Bearer " + this.access_token
      }
    });
    if (response.ok) {
      return response.json().then(response => ({ response }));
    }
    const errorMessage = await response.clone().json();
    if (errorMessage.msg === "Token has expired") {
      await this.update_access_token();
      return this.basicGet(url);
    }
    return response.json().then(error => ({ error }));
  };

  basicPost = async (
    url,
    body = {},
    contentType = "application/json",
    refresh = false
  ) => {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": contentType,
        Authorization: refresh
          ? "Bearer " + this.refresh_token
          : "Bearer " + this.access_token
      },
      body
    });
    if (response.ok) {
      return response.json().then(response => ({ response }));
    }
    const errorMessage = await response.clone().json();
    if (errorMessage.msg === "Token has expired") {
      await this.update_access_token();
      return this.basicPost(url);
    }
    return response.json().then(error => ({ error }));
  };

  // Refactor this to use basicPost
  updateReservation = async (
    reservationId, 
    start, 
    end, 
    refresh = false
    ) => {
    const url = baseUrl + "/reservations/" + reservationId + "/update";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: refresh
          ? "Bearer " + this.refresh_token
          : "Bearer " + this.access_token
      },
      body: JSON.stringify({
        start: start,
        end: end
      })
    });
    if (response.ok) {
      return response.json().then(response => ({ response }));
    }
    return response.json().then(error => ({ error }));
  };
  
  deleteReservation = async (
    reservationId, 
    refresh = false
    ) => {
    const url = baseUrl + "/reservations/" + reservationId + "/delete";
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        Authorization: refresh
          ? "Bearer " + this.refresh_token
          : "Bearer " + this.access_token
      },
    });
    if (response.ok) {
      return response.json().then(response => ({ response }));
    }
    return response.json().then(error => ({ error }));
  };
}
