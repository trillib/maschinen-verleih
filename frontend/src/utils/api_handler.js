
export const baseUrl = "/api/v1";

export class ApiHandler {
  getMachines = async() => {
    return await this.basicGet("/machines");
  }
  
  getMachine = async(id) => {
    return await this.basicGet(`/machines/${id}`);
  }
  
  getReservationsForMachine = async(machineId) => {
    return await this.basicGet(`/machines/${machineId}/reservations`);
  }

  login = async (email, password) => {
    const url = "/login";
    const body = { email, password };
    return this.basicPost(url, JSON.stringify(body));
  }
  
  register = async (name, email, password) => {
    const url = "/register";
    const body = { name, email, password };
    return this.basicPost(url, JSON.stringify(body));
  }

  basicGet = async(url) => {
    const response = await fetch(baseUrl + url);
    if (response.ok) {
      return response.json().then(response => ({ response }));
    }
    return response.json().then(error => ({ error }));
  }

  basicPost = async(url, body, contentType = "application/json") => {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": contentType
      },
      body: body
    });
    if (response.ok) {
      return response.json().then(response => ({ response }));
    }
    return response.json().then(error => ({ error }));
  }
}