import React, { Component } from "react";
import { Container } from "reactstrap";
import { Route } from "react-router-dom";
import Navigation from "./navigation";
import { routes } from "../routes/routes";
import Footer from "./footer";

export default class Layout extends Component {
  render() {
    return (
      <Container>
        <Navigation />
        <br />
        {Object.values(routes).map((route, idx) => {
          return (
            <Route exact key={idx} path={route.path} component={route.component} />
          );
        })}
        <Footer />
      </Container>
    );
  }
}
