import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { routes } from "../routes/routes";
import { Collapse, Navbar, NavbarToggler, Nav, NavItem } from "reactstrap";
import { GlobalContext } from "../providers/providers";
import ReactRouterPropTypes from "react-router-prop-types";
import logo from "../assets/logo.png";

class Navigation extends Component {
  state = { isOpen: false };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  render() {
    return (
      <Navbar color="light" light expand="md">
        <Link className="navbar-brand" to="/">
          <img alt="logo" src={logo} height="50px" />
        </Link>
        <NavbarToggler onClick={this.toggle} />
        <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <GlobalContext.Consumer>
              {context =>
                context.logged_in ? (
                  <Fragment>
                    {context.is_admin && (
                      <NavItem>
                        <Link
                          className="justify-content-center"
                          to={routes.admin_overview.path}
                        >
                          {routes.admin_overview.name}
                        </Link>
                      </NavItem>
                    )}
                    <NavItem>
                      <Link
                        className="justify-content-center"
                        to={routes.user_page.path}
                      >
                        {routes.user_page.name}
                      </Link>
                    </NavItem>
                    <NavItem>
                      <Link to={routes.logout.path}>{routes.logout.name}</Link>
                    </NavItem>
                  </Fragment>
                ) : (
                  <Fragment>
                    <NavItem>
                      <Link to={routes.login.path}>{routes.login.name}</Link>
                    </NavItem>
                    <NavItem>
                      <Link to={routes.register.path}>
                        {routes.register.name}
                      </Link>
                    </NavItem>
                  </Fragment>
                )
              }
            </GlobalContext.Consumer>
          </Nav>
        </Collapse>
      </Navbar>
    );
  }
}
Navigation.propTypes = {
  history: ReactRouterPropTypes.history
};

export default Navigation;
