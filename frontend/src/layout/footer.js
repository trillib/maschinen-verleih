import React from "react";
import {Row, Col} from "reactstrap"

const Footer = () => (
  <Row>
    <Col md="12">
      <div className="footer">
        <p>
          © by my-machine.io, Erstellt von Yvo Brönnimann und Giancarlo Bergamin
        </p>
      </div>
    </Col>
  </Row>
);

export default Footer;