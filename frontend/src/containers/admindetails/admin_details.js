import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import moment from "moment";
import HeadingWithBackArrow from "../../components/common/heading_with_back_arrow";
import { routes } from "../../routes/routes";
import Swal from "sweetalert2";
import AdminDetailTable from "../../components/admindetail/admin_detail_table";
import { AuthorizedApiHandler } from "../../utils/authorized_api_handler";
import { GlobalContext } from "../../providers/providers";

export default class AdminDetails extends Component {
  state = {
    reservations_for_user: [],
    total_cost_for_user: 0,
    username: ""
  };

  retrieveReservations = async () => {
    if (this.props.location.state && this.props.location.state.year) {

      // Get Userid
      this.retrieveUsername()
      var userid
      const detailUserName = this.state.username
      const usersData = await this.authApiHandler.getUsers()
      usersData.response.users.forEach(function(user){
        if (user['name'] === detailUserName){
            userid = user['id']
        }
      })

      const reservations_for_user = await this.authApiHandler.getReservationsForUserAdmin(
        userid,
        this.props.location.state.year
      );
      if (reservations_for_user.response) {
        this.setState({
          reservations_for_user: reservations_for_user.response.reservations
        });
      } else {
        Swal.fire({
          title: "Ein Fehler ist aufgetreten",
          text: reservations_for_user.error.errors,
          type: "error"
        });
      }
    } else {
      Swal.fire({
        title: "Fehler",
        text: "Bitte wählen sie ein Jahr in der vorherigen Ansicht aus",
        type: "error"
      });
    }
  };

  retrieveTotalCost = async () => {
    if (this.props.location.state && this.props.location.state.year) {

      // Get Userid
      this.retrieveUsername()
      var userid
      const detailUserName = this.state.username
      const usersData = await this.authApiHandler.getUsers()
      usersData.response.users.forEach(function(user){
        if (user['name'] === detailUserName){
            userid = user['id']
        }
      })

      const total_cost_for_user = await this.authApiHandler.getTotalCostForUser(
        userid,
        this.props.location.state.year
      );
      if (total_cost_for_user.response) {
        this.setState({
          total_cost_for_user: total_cost_for_user.response.total_cost
        });
      } else {
        Swal.fire({
          title: "Ein Fehler ist aufgetreten",
          text: total_cost_for_user.error.errors,
          type: "error"
        });
      }
    } else {
      Swal.fire({
        title: "Fehler",
        text: "Bitte wählen sie ein Jahr in der vorherigen Ansicht aus",
        type: "error"
      });
    }
  };

  retrieveUsername = async () => {
    if (this.props.location.state && this.props.location.state.username) {
      this.setState({ username: this.props.location.state.username });
    } else {
      const userData = await this.authApiHandler.getUser(
        this.props.match.params.id
      );
      if (userData.response) {
        this.setState({ username: userData.response.user.name });
      }
    }
  };

  componentDidMount = async () => {
    this.authApiHandler = new AuthorizedApiHandler(
      this.context.access_token,
      this.context.refresh_token,
      this.context.refresh_access_token_callback
    );
    await this.retrieveUsername();
    await this.retrieveReservations();
    await this.retrieveTotalCost();
  };

  render() {
    moment.locale("de-ch");
    return (
      <Fragment>
        <Row>
          <Col md="9">
            <HeadingWithBackArrow
              title={this.state.username}
              url={routes.admin_overview.path}
            />
          </Col>
          <Col md="3">
            {this.props.location.state.year && (
              <h1 className="float-right">{this.props.location.state.year}</h1>
            )}
          </Col>
        </Row>
        <br />
        <Row>
          <Col md="12">
            <AdminDetailTable
              reservationData={this.state.reservations_for_user}
              totalCost={this.state.total_cost_for_user}
            />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

AdminDetails.contextType = GlobalContext;
