import React, { Component, Fragment } from "react";
import moment from "moment";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  ModalFooter,
  Input,
  Label,
  FormGroup
} from "reactstrap";
import PropTypes from "prop-types";

class CompleteReservationModal extends Component {
  state = {
    usage: null
  };

  getDisplayText(type) {
    let displayText = "";
    switch (type) {
      case "flaeche":
        displayText = "Geben sie die gefahrene Fläche an (Hektare)";
        break;
      case "ladung":
        displayText = "Geben sie die Anzahl Ladungen an";
        break;
      default:
        displayText = "Wert angeben";
    }
    return displayText;
  }

  render() {
    moment.locale("de-ch")
    const date = moment(this.props.selectedDate).format("LL")
    return (
      <Fragment>
        <Modal isOpen={this.props.isModalOpen}>
          <ModalHeader toggle={this.props.closeModal}>
            {this.props.machineName} - {date}
          </ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="usage">
                {this.getDisplayText(this.props.billingType)}
              </Label>
              <Input
                name="usage"
                id="usage"
                placeholder="12"
                onChange={event => this.setState({ usage: event.target.value })}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button
              color="success"
              onClick={() => this.props.completeReservation(this.state.usage)}
            >
              Reservation abschliessen
            </Button>{" "}
            <Button color="secondary" onClick={this.props.closeModal}>
              Abbrechen
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </Fragment>
    );
  }
}

CompleteReservationModal.propTypes = {
  machineName: PropTypes.string,
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  selectedDate: PropTypes.instanceOf(Date).isRequired,
  completeReservation: PropTypes.func.isRequired,
  billingType: PropTypes.string
};

export default CompleteReservationModal;
