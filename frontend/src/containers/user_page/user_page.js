import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import moment from "moment";
import ReservationCard from "../../components/user_page/reservation_card";
import { GlobalContext } from "../../providers/providers";
import { routes } from "../../routes/routes";
import { Redirect } from "react-router-dom";
import CompleteReservationModal from "./complete_reservation_modal";
import NewReservationModal from "../details/new_reservation_modal";
import Swal from "sweetalert2";
import { AuthorizedApiHandler } from "../../utils/authorized_api_handler";

export default class UserPage extends Component {
  state = {
    preparedReservationData: null,
    selectedReservation: null,
    selectedReservationId: null,
    isCompleteModalOpen: false,
    isUpdateModalOpen: false,
    updateReservation: null
  }

  getTime = datetime => {
    moment.locale("de-ch");
    return moment(datetime).format("LT");
  }

  getDate = datetime => {
    // console.log(datetime)
    // moment.locale("de-ch");
    // return moment(datetime).format("LL");
    return new Date(datetime)
  }

  formatDate = datetime => {
    return moment(datetime).format("LL")
  }

  getType = reservation => {
    if (reservation.status === "abgeschlossen") return "abgeschlossen";
    const startDate = new Date(reservation.end);
    const now = new Date();
    if (startDate > now) {
      return "zukünftig";
    } else {
      return "vergangen";
    }
  }

  openCompleteReservationModal = reservationId => {
    this.setState({
      selectedReservation: this.state.preparedReservationData[reservationId],
      selectedReservationId: reservationId,
      isCompleteModalOpen: true
    });
  }

  openUpdateReservationModal = reservationId => {
    this.setState({
      selectedReservation: this.state.preparedReservationData[reservationId],
      selectedReservationId: reservationId,
      isUpdateModalOpen: true
    });
  }

  completeReservation = async usage => {
    const selectedReservation = this.state.selectedReservation;
    const data = await this.authApiHandler.completeReservation(
      this.state.selectedReservationId,
      usage
    );
    if (data.response) {
      selectedReservation.type = "abgeschlossen";
      Swal.fire({
        title: "Reservation abgeschlossen",
        type: "success",
        timer: 1500,
        showConfirmButton: false
      });
      this.setState(state => {
        return { ...state, selectedReservation, isCompleteModalOpen: false };
      });
    } else {
      Swal.fire({
        title: "Ein Fehler ist aufgetreten",
        text: data.error.errors,
        type: "error"
      });
    }
  }

  closeCompleteReservationModal = () => {
    this.setState({ isCompleteModalOpen: false });
  }

  closeUpdateModal = () => {
    this.setState({ isUpdateModalOpen: false });
  }

  reallyDeleteReservation = async id => {
    let data = await this.authApiHandler.deleteReservation(id);
    return data.response
  }

  deleteReservation = async id => {
    Swal({
      title: "Sind sie sicher?",
      text: "Wollen Sie die Reservierung wirklich löschen?",
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      cancelButtonText: 'Abbrechen',
      confirmButtonText: 'Reservation löschen'
    })
      .then((result) => {
        if (result.value) {
          let resp = this.reallyDeleteReservation(id);
          console.log(resp);
          if (resp) {
            Swal.fire({
              title: "Reservation erfolgreich gelöscht!",
              type: "success",
              timer: 1500,
              showConfirmButton: false
            });
            this.getUpdatedReservations()
          } else {
            Swal.fire({
              title: "Ein Fehler ist aufgetreten!",
              type: "error",
              timer: 1500,
              showConfirmButton: false
            });
          }
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
            title: "Reservation wurde nicht gelöscht!",
            type: "error",
            timer: 1500,
            showConfirmButton: false
          });
        }
      });
  }


  parseReservations = responseData => {
    const prepData = {};
    responseData.reservations.forEach(async reservation => {
      const machineData = await this.authApiHandler.getMachineForReservation(reservation.id);
      let machineName = "N/A";
      let billingType = "N/A";
      if (machineData.response) {
        machineName = machineData.response.machine.name;
        billingType = machineData.response.machine.billing_type;
      }
      prepData[reservation.id] = {
        startDate: this.getDate(reservation.start),
        startTime: this.getTime(reservation.start),
        endTime: this.getTime(reservation.end),
        endDate: this.getDate(reservation.end),
        type: this.getType(reservation),
        machineName: machineName,
        billingType: billingType
      };
      this.setState(state => {
        return { ...state, preparedReservationData: prepData };
      });
    });
  }

  getUpdatedReservations = async () => {
    const data = await this.authApiHandler.getReservationsForUser(this.context.user_id);
    if (data.response) {
      let prepData = {};
      data.response.reservations.forEach(async reservation => {
        const machineData = await this.authApiHandler.getMachineForReservation(reservation.id);
        let machineName = "N/A";
        let billingType = "N/A";
        if (machineData.response) {
          machineName = machineData.response.machine.name;
          billingType = machineData.response.machine.billing_type;
        }
        // If no machineData could be loaded skip this reservation
        else{
          return
        }
        prepData[reservation.id] = {
          startDate: this.getDate(reservation.start),
          startTime: this.getTime(reservation.start),
          endTime: this.getTime(reservation.end),
          endDate: this.getDate(reservation.end),
          type: this.getType(reservation),
          machineName: machineName,
          billingType: billingType
        };
        this.setState(state => {
          return { ...state, preparedReservationData: prepData };
        });
      });
    }
  }


  componentDidMount = async () => {
    this.authApiHandler = new AuthorizedApiHandler(
      this.context.access_token,
      this.context.refresh_token,
      this.context.refresh_access_token_callback
    );
    const data = await this.authApiHandler.getReservationsForUser(this.context.user_id);
    if (data.response) {
      this.parseReservations(data.response);
    }
    this.getUpdatedReservations();
  }


  render() {
    return (
      <GlobalContext.Consumer>
        {context =>
          context.logged_in ? (
            this.state.preparedReservationData ? (
              <Fragment>
                <Row>
                  <Col>
                    <h1>Vergangen</h1>
                  </Col>
                </Row>
                <Row>
                  {this.state.preparedReservationData &&
                    Object.entries(this.state.preparedReservationData)
                      .filter(entry => entry[1].type === "vergangen")
                      .map(entry => {
                        const reservation = entry[1];
                        const id = entry[0];
                        return (
                          <ReservationCard
                            startDate={this.formatDate(reservation.startDate)}
                            endDate={this.formatDate(reservation.endDate)}
                            startTime={reservation.startTime}
                            endTime={reservation.endTime}
                            type={reservation.type}
                            key={id}
                            id={id}
                            machineName={reservation.machineName}
                            completeReservation={
                              this.openCompleteReservationModal
                            }
                            updateReservation={this.openUpdateReservationModal}
                            deleteReservation={this.deleteReservation}
                          />
                        );
                      })}
                </Row>
                <br />
                <Row>
                  <Col>
                    <h1>Zukünftig</h1>
                  </Col>
                </Row>
                <Row>
                  {this.state.preparedReservationData &&
                    Object.entries(this.state.preparedReservationData)
                      .filter(entry => entry[1].type === "zukünftig")
                      .map(entry => {
                        const reservation = entry[1];
                        const id = entry[0];
                        return (
                          <ReservationCard
                            startDate={this.formatDate(reservation.startDate)}
                            endDate={this.formatDate(reservation.endDate)}
                            startTime={reservation.startTime}
                            endTime={reservation.endTime}
                            type={reservation.type}
                            key={id}
                            id={id}
                            machineName={reservation.machineName}
                            completeReservation={
                              this.openCompleteReservationModal
                            }
                            updateReservation={this.openUpdateReservationModal}
                            deleteReservation={this.deleteReservation}
                          />
                        );
                      })}
                </Row>
                <br />
                <Row>
                  <Col>
                    <h1>Abgeschlossen</h1>
                  </Col>
                </Row>
                <Row>
                  {this.state.preparedReservationData &&
                    Object.entries(this.state.preparedReservationData)
                      .filter(entry => entry[1].type === "abgeschlossen")
                      .map(entry => {
                        const reservation = entry[1];
                        const id = entry[0];
                        return (
                          <ReservationCard
                            startDate={this.formatDate(reservation.startDate)}
                            endDate={this.formatDate(reservation.endDate)}
                            startTime={reservation.startTime}
                            endTime={reservation.endTime}
                            type={reservation.type}
                            key={id}
                            id={id}
                            machineName={reservation.machineName}
                            completeReservation={
                              this.openCompleteReservationModal
                            }
                            updateReservation={this.openUpdateReservationModal}
                            deleteReservation={this.deleteReservation}
                          />
                        );
                      })}
                </Row>
                {this.state.selectedReservation && (
                  <CompleteReservationModal
                    machineName={this.state.selectedReservation.machineName}
                    isModalOpen={this.state.isCompleteModalOpen}
                    closeModal={this.closeCompleteReservationModal}
                    selectedDate={this.state.selectedReservation.startDate}
                    completeReservation={this.completeReservation}
                    billingType={this.state.selectedReservation.billingType}
                  />
                )}
                {this.state.selectedReservation && this.state.selectedReservationId && this.state.selectedReservation.startDate && (
                  <NewReservationModal
                    isModalOpen={this.state.isUpdateModalOpen}
                    machineName={this.state.selectedReservation.machineName}
                    closeModal={this.closeUpdateModal}
                    selectedDate={this.state.selectedReservation.startDate}
                    updateModal={true}
                    reservationId={this.state.selectedReservationId}
                    updatedReservationUpdate={this.getUpdatedReservations}
                  />
                )}
              </Fragment>
            ) : (
                <p>Noch keine Reservationen erstellt.</p>
              )
          ) : (
              <Redirect to={routes.login.path} />
            )
        }
      </GlobalContext.Consumer>
    );
  }
}

UserPage.contextType = GlobalContext;
