import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { GlobalContext } from "../../providers/providers";
import { LoginForm } from "../../components/login/login_form";
import { routes } from "../../routes/routes";
import ReactRouterPropTypes from "react-router-prop-types";
import Swal from "sweetalert2";

export default class Login extends Component {
  state = {
    pageToRedirect: routes.home.path
  };

  componentDidMount = () => {
    if (this.props.location.state && this.props.location.state.pageToRedirect) {
      this.setState({
        pageToRedirect: this.props.location.state.pageToRedirect
      });
    }
  };

  submit = ({ fields, isValid }, loginFunction) => {
    if (isValid) {
      loginFunction(fields.email, fields.password).then(response => {
        if (response.response) {
          this.props.history.push(this.state.pageToRedirect);
        } else {
          Swal.fire({
            title: "Fehler",
            text: "E-Mail oder Passwort ist falsch",
            type: "error"
          });
        }
      });
    }
  };

  render() {
    return (
      <Row>
        <Col md="12">
          <GlobalContext.Consumer>
            {({ login }) => (
              <LoginForm submit={result => this.submit(result, login)} />
            )}
          </GlobalContext.Consumer>
        </Col>
      </Row>
    );
  }
}

Login.propTypes = {
  history: ReactRouterPropTypes.history.isRequired,
  location: ReactRouterPropTypes.location.isRequired
};
