import React, { Component, Fragment } from "react";
import { Col, Row } from "reactstrap";
import { RegisterForm } from "../../components/register/register_form";
import { GlobalContext } from "../../providers/providers";
import Swal from "sweetalert2";
import { routes } from "../../routes/routes";


export default class Register extends Component {
  submit = ({ fields, isValid }) => {
    if (isValid) {
      this.context.register(fields.firstname + " " + fields.lastname, fields.email, fields.password).then(response => {
        if(response.response) {
          Swal.fire({
            title: "Erfolgreich registriert",
            type: "success",
            timer: 1500,
            showConfirmButton: false
          });
          this.props.history.push(routes.home.path);
        } else if(response.error) {
          Swal.fire({
            title: "Fehler",
            text: response.error.error,
            type: "error"
          });
        }
      })
    }
  };

  render() {
    return (
      <Fragment>
        <Row>
          <Col>
            <h1>Jetzt Registrieren</h1>
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            <RegisterForm submit={this.submit} />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

Register.contextType = GlobalContext;
