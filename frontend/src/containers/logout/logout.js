import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import { routes } from "../../routes/routes";
import { Link } from "react-router-dom";
import { GlobalContext } from "../../providers/providers";
import ReactRouterPropTypes from 'react-router-prop-types';

export default class Logout extends Component {
  componentDidMount = () => {
      this.context.logout();
      this.props.history.push(routes.logout.path);
  }

  render() {
    return (
      <Row>
        <Col md="12">
          <h1>Sucessfully Logged Out</h1>
          <Link to={routes.home.path}>Zurück zu Home</Link>
        </Col>
      </Row>
    );
  }
}

Logout.propTypes = {
  history: ReactRouterPropTypes.history.isRequired
}
Logout.contextType = GlobalContext