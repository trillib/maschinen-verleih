import React, { Component, Fragment } from "react";
import { Modal, ModalBody, ModalHeader, Button, ModalFooter } from "reactstrap";
import PropTypes from "prop-types";
import ReservationForm from "../../components/details/reservation_form";
import { AuthorizedApiHandler } from "../../utils/authorized_api_handler";
import Swal from "sweetalert2";
import moment from "moment";
import "moment/locale/de";
import { GlobalContext } from "../../providers/providers";

class NewReservationModal extends Component {
  state = {
    fromDate: null,
    toDate: null,
    fromTime: null,
    toTime: null
  };

  componentWillReceiveProps = () => {
    console.log("Bla:" + this.props.selectedDate)
    if (this.props.selectedDate !== "") {
      try {
        console.log("WillRecieveProps " + this.props.selectedDate)
        const date = new Date(this.props.selectedDate);
        
        this.setState({
          fromDate: date,
          toDate: date
        });
      } catch (ex) {
        console.ex();
      }
    }
  };

  combineDateAndTime = (date, time) => {
    const timeString = time.getHours() + ":" + time.getMinutes() + ":00";
    const year = date.getFullYear();
    const month = date.getMonth() + 1; // Jan is 0, dec is 11
    const day = date.getDate();
    const dateString = "" + year + "-" + month + "-" + day;
    const combined = new Date(dateString + " " + timeString);
    return combined;
  };

  submitReservation = async () => {
    const fromDate = this.combineDateAndTime(
      this.state.fromDate,
      this.state.fromTime
    );
    const toDate = this.combineDateAndTime(
      this.state.toDate,
      this.state.toTime
    );
    const authApiHandler = new AuthorizedApiHandler(
      this.context.access_token,
      this.context.refresh_token,
      this.context.refresh_access_token_callback
    );
    const data = await authApiHandler.createNewReservation(
      this.props.machineId,
      fromDate,
      toDate
    );
    if (data.response) {
      this.props.closeModal();
      Swal.fire({
        title: "Reservation wurde erstellt",
        type: "success",
        timer: 1500,
        showConfirmButton: false
      });
    } else {
      Swal.fire({
        title: "Ein Fehler ist aufgetreten",
        text: data.error.errors,
        type: "error"
      });
    }
  };

  updateReservation = async () => {
    const fromDate = this.combineDateAndTime(
      this.state.fromDate,
      this.state.fromTime
    );
    const toDate = this.combineDateAndTime(
      this.state.toDate,
      this.state.toTime
    );
    const authApiHandler = new AuthorizedApiHandler(
      this.context.access_token,
      this.context.refresh_token,
      this.context.refresh_access_token_callback
    );
    const data = await authApiHandler.updateReservation(
      this.props.reservationId,
      fromDate,
      toDate
    );
    if (data.response) {
      this.props.closeModal();
      Swal.fire({
        title: "Reservation wurde upgedated",
        type: "success",
        timer: 1500,
        showConfirmButton: false
      });
      this.props.updatedReservationUpdate();
    } else {
      Swal.fire({
        title: "Ein Fehler ist aufgetreten",
        text: data.error.errors,
        type: "error"
      });
    }
  };

  handleChange = (property, value) => {
    this.setState({ [property]: value });
  };

  render() {
    moment.locale("de-ch");
    const date = moment(this.props.selectedDate).format("LL");
    return (
      <Fragment>
        <Modal isOpen={this.props.isModalOpen}>
          <ModalHeader toggle={this.props.closeModal}>
            {this.props.machineName} - {date}
          </ModalHeader>
          <ModalBody>
            <ReservationForm
              fromDate={this.state.fromDate}
              toDate={this.state.toDate}
              handleChange={this.handleChange}
              fromTime={this.state.fromTime}
              toTime={this.state.toTime}
            />
          </ModalBody>
          <ModalFooter>
            { this.props.updateModal ? (
            <Button color="success" onClick={() => this.updateReservation()}>
              Reservation updaten
            </Button>) : (
            <Button color="success" onClick={() => this.submitReservation()}>
              Reservation erstellen
            </Button>)}{" "}
            <Button color="secondary" onClick={this.props.closeModal}>
              Abbrechen
            </Button>{" "}
          </ModalFooter>
        </Modal>
      </Fragment>
    );
  }
}

NewReservationModal.propTypes = {
  machineName: PropTypes.string,
  updateModal: PropTypes.bool,
  machineId: PropTypes.number,
  reservationId: PropTypes.string.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  selectedDate: PropTypes.instanceOf(Date).isRequired,
  updatedReservationUpdate: PropTypes.func.isRequired
};

NewReservationModal.contextType = GlobalContext;
export default NewReservationModal;
