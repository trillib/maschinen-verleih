import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import ReactRouterPropTypes from "react-router-prop-types";
import ReservationCalendar from "../../components/details/reservation_calendar";
import ReservationOverviewModal from "../../components/details/reservation_overview_modal";
import NewReservationModal from "./new_reservation_modal";
import { routes } from "../../routes/routes";
import { ApiHandler} from "../../utils/api_handler";
import moment from "moment";
import "moment/locale/de";
import { GlobalContext } from "../../providers/providers";
import HeadingWithBackArrow from "../../components/common/heading_with_back_arrow";

export default class MachineDetails extends Component {
  state = {
    overviewModalOpen: false,
    newReservationModalOpen: false,
    currentReservationDay: "",
    machine: null,
    reservations: null,
    isLoading: true
  };

  openOverviewModal = day => {
    this.setState({
      currentReservationDay: new Date(day).toISOString(),
      overviewModalOpen: true
    });
  };

  newReservation = logged_in => {
    if (logged_in) {
      this.setState({
        overviewModalOpen: false,
        newReservationModalOpen: true
      });
    } else {
      this.props.history.push({
        pathname: routes.login.path,
        state: { pageToRedirect: window.location.pathname }
      });
    }
  };

  closeModal = () => {
    this.setState({
      overviewModalOpen: false,
      newReservationModalOpen: false,
      currentReservationDay: ""
    });
  };

  submitReservation = data => {
    this.setState({
      newReservationModalOpen: false,
      currentReservationDay: ""
    });
  };

  parseReservations = reservationData => {
    const reservations = [];

    console.log(reservationData)
    reservationData.forEach(reservation => {
      const start = moment(reservation.start);
      const end = moment(reservation.end);
      const username = reservation.username;

      if (start.date() === end.date()) {
        const reservation = {
          time: start.format("HH:mm") + "-" + end.format("HH:mm"),
          date: start.format("DD.MM.YYYY"),
          reserved_by: username,
          duration: end.hour() + 1 - start.hour()
        };
        reservations.push(reservation);
      } else {
        const first_day_reservation = {
          time: start.format("HH:mm") + " - 00:00",
          date: start.format("DD.MM.YYYY"),
          reserved_by: username,
          duration: 24 - start.hour()
        };
        reservations.push(first_day_reservation);
        const second_day_reservation = {
          time: "00:00 - " + end.format("HH:mm"),
          date: end.format("DD.MM.YYYY"),
          reserved_by: username,
          duration: end.hour()
        };
        reservations.push(second_day_reservation);
      }
    });
    return reservations;
  };

  componentDidMount = async () => {
    moment.locale("de-ch");
    const id = this.props.match.params.id;
    const apiHandler = new ApiHandler()
    let machine = await this.getMachine(id, apiHandler);
    const reservationsData = await apiHandler.getReservationsForMachine(machine.id);
    const reservations = this.parseReservations(
      reservationsData.response.reservations
    );
    this.setState({
      machine: machine,
      reservations: reservations,
      isLoading: false
    });
  };

  getMachine = async (id, apiHandler) => {
    //Only get data from server when there is none provided in link
    if (this.props.location.state && this.props.location.state.machine) {
      return this.props.location.state.machine;
    } else {
      const machineData = await apiHandler.getMachine(id);
      if (machineData.response) {
        return machineData.response.machine;
      }
    }
  };

  render() {
    return !this.state.isLoading ? (
      <Fragment>
        <Row>
          <Col md="6">
            <Row>
              <Col md="12">
                <HeadingWithBackArrow
                  title={this.state.machine && this.state.machine.name}
                  url={routes.home.path}
                />
                <h4>
                  {" "}
                  <i className="fa fa-home" />{" "}
                  {this.state.machine && this.state.machine.location}
                </h4>
              </Col>
            </Row>
            <Row>
              <Col>
                <p>{this.state.machine && this.state.machine.description}</p>
              </Col>
            </Row>
          </Col>
          <Col md="6">
            {this.state.reservations && (
              <ReservationCalendar
                onClick={this.openOverviewModal}
                reservations={this.state.reservations}
              />
            )}
          </Col>
        </Row>
        <GlobalContext.Consumer>
          {context =>
            this.state.reservations && (
              <ReservationOverviewModal
                isModalOpen={this.state.overviewModalOpen}
                closeModal={this.closeModal}
                newReservation={() => this.newReservation(context.logged_in)}
                reservations={this.state.reservations}
                machineName={this.state.machine && this.state.machine.name}
                selectedDate={moment(this.state.currentReservationDay).format(
                  "Do MMMM YYYY"
                )}
                selDate={moment(this.state.currentReservationDay).format(
                  "DD.MM.YYYY"
                )}
              />
            )
          }
        </GlobalContext.Consumer>
        <NewReservationModal
          isModalOpen={this.state.newReservationModalOpen}
          machineName={this.state.machine && this.state.machine.name}
          machineId={this.state.machine && this.state.machine.id}
          closeModal={this.closeModal}
          selectedDate={this.state.currentReservationDay}
          selDate={moment(this.state.currentReservationDay).format(
            "DD.MM.YYYY"
          )}
        />
      </Fragment>
    ) : null;
  }
}

MachineDetails.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
  location: ReactRouterPropTypes.location.isRequired
};
