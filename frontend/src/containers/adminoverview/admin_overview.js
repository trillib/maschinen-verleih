import React, { Component, Fragment } from "react";
import { Row, Col } from "reactstrap";
import ReactRouterPropTypes from "react-router-prop-types";
import YearDropdown from "../../components/adminoverview/year_dropdown";
import UserTable from "../../components/adminoverview/user_table";
import StatisticsBlock from "../../components/adminoverview/statistics_block";
import { AuthorizedApiHandler } from "../../utils/authorized_api_handler";
import { GlobalContext } from "../../providers/providers";

export default class AdminOverview extends Component {
  state = {
    dropdownOpen: false,
    userTableData: null,
    yearsData: [],
    selectedYear: 2019,
    revenue: 0,
    numberOfUsers: 0
  };

  retrieveUsers = async () => {
    const usersData = await this.authApiHandler.getUsers();
    if (usersData.response) {
      this.setState({ userTableData: usersData.response.users });
    }
  };

  retrieveYearsList = async () => {
    const yearsData = await this.authApiHandler.getYears();
    if (yearsData.response) {
      const currentYear = yearsData.response.years[0];
      this.setState({
        yearsData: yearsData.response.years,
        selectedYear: currentYear
      });
      return currentYear;
    }
    return null;
  };

  retrieveStatisticsData = async currentYear => {
    const statisticsData = await this.authApiHandler.getStatistics(currentYear);
    if (statisticsData.response) {
      this.setState({
        revenue: ~~statisticsData.response.statistics.revenue,
        numberOfUsers: ~~statisticsData.response.statistics.number_of_users
      });
    }
  };

  componentDidMount = async () => {
    this.authApiHandler = new AuthorizedApiHandler(
      this.context.access_token,
      this.context.refresh_token,
      this.context.refresh_access_token_callback
    );
    await this.retrieveUsers();
    const currentYear = await this.retrieveYearsList();
    if (currentYear !== null) {
      await this.retrieveStatisticsData(currentYear);
    }
  };

  componentWillUpdate = async (nextProps, nextState) => {
    const selected_year = nextState.selectedYear;
    if (this.state.selectedYear !== selected_year) {
      const statisticsData = await this.authApiHandler.getStatistics(selected_year);
      if (statisticsData.response) {
        this.setState({
          revenue: ~~statisticsData.response.statistics.revenue,
          numberOfUsers: ~~statisticsData.response.statistics.number_of_users
        });
      }
    }
  };

  toggleDropdown = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  setDropdownValue = value => {
    this.setState({ selectedYear: ~~value });
  };

  redirectToUserPage = (id, username) => {
    this.props.history.push({
      pathname: `/admin/${id}`,
      state: { username, year: this.state.selectedYear }
    });
  };

  render() {
    return (
      <Fragment>
        <Row>
          <Col md="9">
            <Row>
              <Col md="12">
                <YearDropdown
                  dropdownItems={this.state.yearsData}
                  dropdownOpen={this.state.dropdownOpen}
                  toggleDropdown={this.toggleDropdown}
                  setDropdownValue={this.setDropdownValue}
                  selectedDropdownValue={this.state.selectedYear}
                />
                <h1>Admin Panel</h1>
              </Col>
            </Row>
            <br />
            <Row>
              <Col md="12">
                {this.state.userTableData && (
                  <UserTable
                    detailAction={this.redirectToUserPage}
                    tableData={this.state.userTableData}
                  />
                )}
              </Col>
            </Row>
          </Col>
          <Col md="3">
            <Row>
              <Col md="12">
                <StatisticsBlock
                  revenue={this.state.revenue}
                  userCount={this.state.numberOfUsers}
                />
              </Col>
            </Row>
          </Col>
        </Row>
        <br />
      </Fragment>
    );
  }
}

AdminOverview.contextType = GlobalContext;

AdminOverview.propTypes = {
  history: ReactRouterPropTypes.history.isRequired
};
