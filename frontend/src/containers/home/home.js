import React, { Component, Fragment } from "react";
import { Col, Row } from "reactstrap";
import SearchForm from "../../components/home/search_form";
import HomeList from "../../components/home/home_list";
import { ApiHandler } from "../../utils/api_handler";
import { GlobalContext } from "../../providers/providers";

export default class Home extends Component {
  state = {
    inital_machine_items: [],
    machine_items: []
  };

  search = input => {
    let currentList = [];
    let newList = [];
    let searchValue = input.target.value;

    if (searchValue !== "") {
      currentList = this.state.inital_machine_items;
      newList = currentList.filter(item => {
        const lc = item.name.toLowerCase();
        const filter = searchValue.toLowerCase();
        return lc.includes(filter);
      });
    } else {
      newList = this.state.inital_machine_items;
    }
    this.setState({
      machine_items: newList
    });
  };

  async componentDidMount() {
    const data = await new ApiHandler().getMachines();
    if (data.response) {
      this.setState({
        machine_items: data.response.machines,
        inital_machine_items: data.response.machines
      });
    }
  }

  render() {
    return (
      <Fragment>
        <Row>
          <Col md="6">
            <HomeList items={this.state.machine_items} />
          </Col>
          <Col md="6">
            <SearchForm searchFunction={this.search} />
          </Col>
        </Row>
      </Fragment>
    );
  }
}

Home.contextType = GlobalContext;
