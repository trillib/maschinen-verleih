import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Layout from "./layout";
import Provider from "./providers/providers";
import "./styles/global.css";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Provider>
          <Route path="/" name="Home" component={Layout} />
        </Provider>
      </BrowserRouter>
    );
  }
}

export default App;
