import React, { Component } from "react";
import PropTypes from "prop-types";
import { ApiHandler } from "../utils/api_handler";
import { AuthorizedApiHandler } from "../utils/authorized_api_handler";

const DEFAULT_STATE = {
  logged_in: false,
  access_token: "",
  refresh_token: "",
  user_id: -1,
  is_admin: false
};

export const GlobalContext = React.createContext(DEFAULT_STATE);

export default class Provider extends Component {
  state = DEFAULT_STATE;
  login = (email, password) => {
    return new ApiHandler().login(email, password).then(response => {
      if (response.response) {
        this.setAuthenticationInformation(response);
      }
      return response;
    });
  };

  register = (name, email, password) => {
    return new ApiHandler().register(name, email, password).then(response => {
      if (response.response) {
        this.setAuthenticationInformation(response);
      }
      return response;
    });
  };

  setAuthenticationInformation = response => {
    localStorage.setItem("access_token", response.response.access_token);
    localStorage.setItem("refresh_token", response.response.refresh_token);
    localStorage.setItem("user_id", response.response.user_id);
    localStorage.setItem("is_admin", response.response.is_admin);
    this.setState({
      logged_in: true,
      access_token: response.response.access_token,
      refresh_token: response.response.refresh_token,
      user_id: response.response.user_id,
      is_admin: response.response.is_admin
    });
  };

  logout = () => {
    const authApiHandler = new AuthorizedApiHandler(
      this.state.access_token,
      this.state.refresh_token,
      this.refresh_access_token_callback
    );
    authApiHandler.logout().then(response => {
      if (response.response) {
        localStorage.clear();
        this.setState({
          logged_in: false,
          access_token: "",
          refresh_token: "",
          user_id: -1,
          is_admin: false
        });
      }
    });
  };

  refresh_access_token_callback = new_access_token => {
    this.setState({ access_token: new_access_token });
  };

  componentWillMount = () => {
    const access_token = localStorage.getItem("access_token");
    const refresh_token = localStorage.getItem("refresh_token");
    const user_id = localStorage.getItem("user_id");
    const is_admin = localStorage.getItem("is_admin") === "false" ? false : true;
    if (access_token !== null) {
      this.setState({
        access_token
      });
    }
    if (user_id !== null) {
      this.setState({
        user_id
      });
    }
    if (is_admin !== null) {
      this.setState({
        is_admin
      });
    }
    if (refresh_token !== null) {
      this.setState({
        refresh_token,
        logged_in: true
      });
    }
  };

  render() {
    return (
      <GlobalContext.Provider
        value={{
          ...this.state,
          login: this.login,
          logout: this.logout,
          refresh_access_token_callback: this.refresh_access_token_callback,
          register: this.register
        }}
      >
        {this.props.children}
      </GlobalContext.Provider>
    );
  }
}

Provider.propTypes = {
  children: PropTypes.any
};
