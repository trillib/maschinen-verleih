import Home from "../containers/home/home";
import Login from "../containers/login/login";
import Logout from "../containers/logout/logout";
import Machine from "../containers/details/machine_details";
import UserPage from "../containers/user_page/user_page";
import AdminOverview from "../containers/adminoverview/admin_overview";
import AdminDetails from "../containers/admindetails/admin_details";
import Register from "../containers/register/register";

export const routes = {
  home: {
    component: Home,
    in_navigation: true,
    name: "Home",
    path: "/"
  },
  login: {
    path: "/login",
    component: Login,
    in_navigation: true,
    name: "Login"
  },
  logout: {
    path: "/logout",
    component: Logout,
    in_navigation: false,
    name: "Logout"
  },
  machine: {
    path: "/machines/:id",
    component: Machine,
    in_navigation: false,
    name: "Machine"
  },
  user_page: {
    path: "/user",
    component: UserPage,
    in_navigation: true,
    name: "User"
  },
  admin_overview: {
    path: "/admin",
    component: AdminOverview,
    in_navigation: false,
    name: "Admin"
  },
  admin_details: {
    path: "/admin/:id",
    component: AdminDetails,
    in_navigation: false,
    name: "Admin Detail"
  },
  register: {
    path: "/register",
    component: Register,
    in_navigation: false,
    name: "Register"
  }
};
